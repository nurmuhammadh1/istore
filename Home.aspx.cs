﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace ISTORE
{
    public partial class Home : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;



        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            ShowData();
        }



        public void ShowData()
        {
            conn.Open();
            try
            {
                sql = "select *, BENEFIT.DESCRIPTION FROM PRODUCT INNER JOIN BENEFIT ON PRODUCT.BENEFITID = BENEFIT.BENEFITID ";
                cmd = new SqlCommand(sql, conn);
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowProduct.DataSource = dt;
                ShowProduct.DataBind();
            }
            catch (SqlException ex)
            {

            }
            finally
            {
                conn.Close();
            }
        }

        protected void AddToCart(decimal QUANTITY, string PRODUCTID, int USERID)
        {
            conn.Open();
            try
            {


                sql = "SELECT * FROM CART WHERE PRODUCTID = '" + PRODUCTID + "' AND USERID = "+USERID+"  ";
                cmd = new SqlCommand(sql, conn);
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO CART VALUES(" + QUANTITY + ", '" + PRODUCTID + "', " + USERID + ")";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
                else
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE CART SET QUANTITY = " + QUANTITY + " WHERE PRODUCTID =  '" + PRODUCTID + "'";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "warningalert();", true);

                }


            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string NAME = Name.Text;
            string USERID = Session["USERID"].ToString();
            AddToCart(1, NAME, Convert.ToInt32(USERID));
            ShowData();
        }


    }
}