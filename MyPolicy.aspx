﻿<%@ Page Title="My Policy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyPolicy.aspx.cs" Inherits="ISTORE.MyPolicy" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="mt-4"><%: Title %>.</h2>

    <div class="row mt-3">
        <asp:ListView ID="ShowPolicy" runat="server">
            <LayoutTemplate>
                <table class="table table-striped">
                    <tr>
                        <td>POLICY NO</td>
                        <td>PRODUCT NAME</td>
                        <td>INSURED NAME</td>
                        <td>STATUS POLICY</td>
                        <td>PREMIUM</td>
                        <td>ACTION</td>
                    </tr>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td><%# Eval("PRODUCTID") %>-<%# Eval("POLICYNO") %></td>
                    <td><%# Eval("NAME") %></td>
                    <td><%# Eval("INSUREDNAME") %></td>
                    <td><%# Eval("STATUS") %></td>
                    <td><%# Eval("PREMIUM") %></td>
                    <td>
                        <%  conn.Open();
                            cmd.CommandText = "select COUNT(*) FROM POLICY WHERE PAYMENTFILE = '' AND USERID = "+Session["USERID"]+"";
                            int stat = Convert.ToInt32(cmd.ExecuteScalar());
                            
                            if (stat == 0)
                            { %>
                                <p>You has been Uploaded</p>
                        <%}
                            else
                            {  %>
                        <a id="edit_Policy" class="btn btn-primary" data-toggle="modal" data-policyno="<%# Eval("POLICYNO") %>" data-target="#exampleModal">Upload</a>
                        <% } conn.Close(); %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-primary">
                    <h5 class="modal-title" id="exampleModalLabel">Upload Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:Label hidden for="id" ID="Label3" runat="server" Text="Name Product"></asp:Label>
                        <asp:TextBox hidden ClientIDMode="Static" ID="Policyno" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label for="id" ID="Label1" runat="server" Text="Payment Proof"></asp:Label>
                        <asp:TextBox type="file" ID="Payment" CssClass="form-control mt-2" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label hidden ID="Label2" runat="server" Text="Status"></asp:Label>
                        <asp:TextBox hidden ID="Status" CssClass="form-control" runat="server" Text="Inforce"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnUpload" OnClick="btnUpload_Click" CssClass="btn btn-primary" runat="server" Text="Upload" />
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#myPolicy').DataTable();
        });
        $(document).on("click", "#edit_Policy", function () {
            var _policyno = $(this).data("policyno");

            $("#exampleModal #Policyno").val(_policyno);
            $("#exampleModal #Policyno").attr("ReadOnly", true);

        })
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function successalert() {
            swal({
                icon: 'success',
                title: 'Congratulation',
                text: 'Congratulation your policy has been inforce',
                type: 'success'
            });
        }
    </script>

    <script type="text/javascript">
        function warningalert() {
            swal({
                icon: 'warning',
                title: 'Warning',
                text: 'You do not have to upload payment, your status policy is inforce',
                type: 'warning'
            });
        }
    </script>

    <script type="text/javascript">
        function informationalert() {
            swal({
                icon: 'info',
                title: 'Information',
                text: 'If your policy inforce you dont have to upload payment',
                type: 'info'
            });
        }
    </script>

</asp:Content>
