﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="ISTORE.Home" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">   
    <div class="jumbotron">
        <h1>ISTORE</h1>
        <h2>Hello, <%=Session["USERNAME"]  %>  we hope you are happy today. </h2>
        <img src="images/iss.jpg" class="card-img-top" width="100%">
    </div>

    <asp:Label ID="userid" runat="server"></asp:Label>
    

    <div class="row">
        <asp:Repeater ID="ShowProduct" runat="server">
            <ItemTemplate>
                <div id="myCard" class="col-md-4">
                    <img src="images/<%#Eval("IMAGES") %>" class="card-img-top" width="100%">
                    <h2><%#Eval("NAME") %></h2>
                    <p>
                        <%#Eval("DESCRIPTION") %>
                    </p>

                    <% if (Session["USERID"] != null)
                        {  %>
                    <p runat="server">
                        <a class="btn btn-success" href="DetailsProduct.aspx?PRODUCTID=<%# Eval("PRODUCTID") %>">View</a>
                        <a id="Add_Cart" data-id="<%# Eval("PRODUCTID") %>" data-nama="<%# Eval("NAME") %>" class="btn btn-primary"
                            data-toggle="modal" data-target="#exampleModal">Add To Cart</a>
                    </p>
                    <%}else{ %>
                    <p runat="server">
                        <a class="btn btn-success" href="DetailsProduct.aspx?PRODUCTID=<%# Eval("PRODUCTID") %>">View</a>
                    </p>
                    <%} %>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-primary">
                    <h5 class="modal-title" id="exampleModalLabel">Add To Cart</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:Label hidden for="id" ID="Label3" runat="server" Text="Name Product"></asp:Label>
                        <asp:TextBox hidden ClientIDMode="Static" ID="Name" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label for="id" ID="Label1" runat="server" Text="Name Product"></asp:Label>
                        <asp:TextBox ClientIDMode="Static" ID="productid" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Quantity"></asp:Label>
                        <asp:TextBox type="number" ReadOnly="true" ID="Quantity" CssClass="form-control" runat="server" Text="1"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" runat="server" Text="Add To Cart" />
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('#myCard').DataTable();
        });
        $(document).on("click", "#Add_Cart", function () {
            var _id = $(this).data("id");
            var _name = $(this).data("nama");
            

            $("#exampleModal #Name").val(_id);
            $("#exampleModal #Name").attr("ReadOnly", true);
            $("#exampleModal #productid").val(_name);
            $("#exampleModal #productid").attr("ReadOnly", true);
        })
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function successalert() {
            swal({
                icon: 'success',
                title: 'Success',
                text: 'This product has been add to cart, please checkout your product',
                type: 'success'
            }).then(function () {
                window.location = "viewcart.aspx"
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                icon: 'warning',
                title: 'Warning',
                text: 'Your can buy the product just one, please checkout your product',
                type: 'warning'
            }).then(function () {
                window.location = "viewcart.aspx"
            });
        }
    </script>
</asp:Content>
