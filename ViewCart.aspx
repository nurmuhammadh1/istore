﻿<%@ Page Title="Shopping Cart" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewCart.aspx.cs" CodeFile="ViewCart.aspx.cs" Inherits="ISTORE.ViewCart" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1 class="mt-4">Shopping Cart</h1>
        <div class="mt-3">
        <a runat="server" href="~/Home">< Back to Home ></a>
        <a runat="server" href="~/MyPolicy">< Check My Policy ></a>
        </div>
        <br />
        <br />
        <div class="row">
            <asp:ListView ID="ShowCart" runat="server">
                <LayoutTemplate>
                    <table class="table table-striped">
                        <tr>
                            <td>PRODUCT ID</td>
                            <td>NAME</td>
                            <td>QUANTITY</td>
                            <td>ACTION</td>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr runat="server">
                        <td id="PRODUCTID"><%# Eval("PRODUCTID") %></td>
                        <td><%# Eval("NAME") %></td>
                        <td><%# Eval("QUANTITY") %></td>
                        <td>
                            <a id="Remove_Cart" data-id="<%# Eval("NAME") %>" data-cart="<%# Eval("CARTID") %>" class="btn btn-danger"
                                data-toggle="modal" data-target="#exampleModal">Remove</a>
                            <a href="CheckOut.aspx?PRODUCTID=<%# Eval("PRODUCTID") %>" class="btn btn-primary">Check Out</a>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
            <br />
            <%--<asp:Button runat="server" ID="btnUpdateCart" Text="Update Cart" OnClick="btnUpdateCart_Click" />--%>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Are you sure want to delete?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <asp:Label hidden  ID="Label1" runat="server" Text="CART ID"></asp:Label>
                            <asp:TextBox hidden  ClientIDMode="Static" ID="Cartid" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="PRODUCT NAME"></asp:Label>
                            <asp:TextBox ClientIDMode="Static" ID="Name" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <asp:Button ID="btnRemove" OnClick="btnRemove_Click" CssClass="btn btn-danger" runat="server" Text="Remove" />
                    </div>
                    </div>
                </div>
            </div>
        </div>
    <script>
        $(document).ready(function () {
            $('#myCard').DataTable();
        });
        $(document).on("click", "#Remove_Cart", function () {
            var _id = $(this).data("id");
            var _cartid = $(this).data("cart");

            $("#exampleModal #Cartid").val(_cartid);
            $("#exampleModal #Cartid").attr("ReadOnly", true);
            $("#exampleModal #Name").val(_id);
            $("#exampleModal #Name").attr("ReadOnly", true);
        })
    </script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    

   <script type="text/javascript">
       function successalert() {
           swal({
               icon: 'info',
               title: 'Removed',
               text: 'Your product in cart has been removed from shopping cart',
               type: 'success'
           });
       }
   </script>
    
</asp:Content>
