﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace ISTORE
{
    public partial class _Default : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;

        

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            ShowData();
        }


        public void ShowData()
        {
            conn.Open();
            try
            {
                sql = "select *, BENEFIT.DESCRIPTION FROM PRODUCT INNER JOIN BENEFIT ON PRODUCT.BENEFITID = BENEFIT.BENEFITID ";
                cmd = new SqlCommand(sql, conn);
                
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowProduct.DataSource = dt;
                ShowProduct.DataBind();
            }
            catch (SqlException ex)
            {

            }
            finally
            {
                conn.Close();
            }
        }

    }
}