﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="ISTORE.Account" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron bg-white" style="border:5px solid #00ffff">
        <div class="title mb-5">
            <center>
                <h1>Your Account</h1>
            </center>
        </div>
        <div class="form-row">
            <asp:Repeater ID="ShowUser" runat="server">
                <ItemTemplate>
                    <div class="form-group mt-2 col-md-4">
                        <label>Full Name</label>
                        <input type="text" readonly  value="<%#Eval("USERNAME") %>" class="form-control">
                    </div>
                    
                    <div class="form-group mt-2 col-md-4">
                        <label>Email</label>
                        <input type="text" id="Email" readonly value="<%#Eval("EMAIL") %>" class="form-control">
                    </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Birth Place</label>
                            <input type="text" id="BirthPlace" readonly value="<%#Eval("BIRTHPLACE") %>" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Password</label>
                            <input type="password" readonly value="<%#Eval("PASSWORD") %>" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Birth Date</label>
                        <input type="date" id="BirthDate" readonly value="<%#Eval("BIRTHDATE") %>" class="form-control">
                    </div>
                        <div class="form-group col-md-3 ml-3">
                        <a id="Update_User" class="btn btn-primary float-right" data-userid="<%#Eval("USERID") %>" data-username="<%#Eval("USERNAME") %>"
                            data-email="<%#Eval("EMAIL") %>" data-password="<%#Eval("PASSWORD") %>"
                            data-toggle="modal" data-target="#Edit_Account">Update Account</a>
                        
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="inputState">Gender</label>
                        <input type="text" id="Gender" readonly value="<%#Eval("GENDER") %>" class="form-control">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlTextarea1">Address</label>
                            <textarea class="form-control" readonly id="Address" rows="3"><%#Eval("ADDRESS") %></textarea>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Zip Code</label>
                            <input type="text" id="ZipCode" readonly value="<%#Eval("ZIPCODE") %>" class="form-control">
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="col-md-12 text-right">
                <a class="btn btn-success" runat="server" href="~/Home">Back Home</a>
            </div>
        </div>

       <!-- Modal -->
    <div class="modal fade" id="Edit_Account" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-primary">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:Label for="id" hidden ID="Label4" runat="server" Text="userid"></asp:Label>
                        <asp:TextBox type="text" hidden ClientIDMode="Static"  ID="Userid" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label for="id" ID="Label3" runat="server" Text="FULL NAME"></asp:Label>
                        <asp:TextBox type="text" ClientIDMode="Static" required ID="Username" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label for="id" ID="Label1" runat="server" Text="EMAIL"></asp:Label>
                        <asp:TextBox type="email" ClientIDMode="Static" required ID="Email" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="PASSWORD"></asp:Label>
                        <asp:TextBox required ClientIDMode="Static" type="text" ID="Password" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnEditUser" OnClick="btnEditUser_Click" CssClass="btn btn-primary" runat="server" Text="Edit User" />
                </div>
            </div>
        </div>
    </div>
  
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <script type="text/javascript">
            function successalert() {
                swal({
                    icon: 'success',
                    title: 'Success',
                    text: 'Succes, your account has been updated',
                    type: 'success'
                });
            }
        </script>
        <script type="text/javascript">
            function erroralert() {
                swal({
                    icon: 'error',
                    title: 'Error',
                    text: 'The email already registed, please use another email',
                    type: 'error'
                });
            }
        </script>

        <script>
            $(document).ready(function () {
                $('#myUser').DataTable();
            });
            $(document).on("click", "#Update_User", function () {
                var _userid = $(this).data("userid");
                var _username = $(this).data("username");
                var _email = $(this).data("email");
                var _password = $(this).data("password");


                $("#Edit_Account #Userid").val(_userid);
                $("#Edit_Account #Username").val(_username);
                $("#Edit_Account #Email").val(_email);
                $("#Edit_Account #Password").val(_password);
            })
        </script>
  
</asp:Content>



