﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace ISTORE
{
    public partial class SiteMaster : MasterPage
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
        }

        protected void Login(string EMAIL, string PASSWORD)
        {
            conn.Open();
            try
            {


                sql = "SELECT * FROM [USER] WHERE EMAIL = '" + EMAIL + "' AND PASSWORD = '" + PASSWORD + "' ";
                cmd = new SqlCommand(sql, conn);
                SqlDataReader idr = cmd.ExecuteReader();
               
                

                if (idr.Read())
                {
                    Session["USERID"] = idr["USERID"];
                    Session["EMAIL"] = idr["EMAIL"];
                    Session["USERNAME"] = idr["USERNAME"] ;
                    Response.Redirect("Home.aspx");
                    Session.RemoveAll();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }



        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Session["EMAIL"] = Email.Text;
            string EMAIL = Email.Text;
            string PASSWORD = Password.Text;
            Login(EMAIL, PASSWORD);
        }

        protected void LogOut_Click(object sender, EventArgs e)
        {
            Session.Remove("USERID");
            Session.Remove("EMAIL");
            Response.Redirect("~/");
        }
    }
}