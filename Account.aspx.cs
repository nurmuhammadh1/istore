﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISTORE
{
    public partial class Account : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            if (Session["EMAIL"] != null)
            {
                ShowData();
            }
        }

        public void ShowData()
        {
            conn.Open();
            try
            {
                sql = "select * from [USER] WHERE USERID = " + Session["USERID"] + "";
                cmd = new SqlCommand(sql, conn);
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowUser.DataSource = dt;
                ShowUser.DataBind();
            }
            catch (SqlException ex)
            {

            }
            finally
            {
                conn.Close();
            }
        }

        

        protected void UpdateUser(string USERNAME, string EMAIL, string PASSWORD )
        {
            conn.Open();
            try
            {
                cmd.CommandText = "SELECT COUNT(*) FROM [USER] WHERE USERID = "+Session["USERID"]+"";
                int validation = Convert.ToInt32(cmd.ExecuteScalar());

                if (validation == 1)
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE [USER] SET USERNAME = '" + USERNAME + "', PASSWORD = '" + PASSWORD + "', EMAIL = '" + EMAIL + "' WHERE USERID = " + Session["USERID"] + " ";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        protected void btnEditUser_Click(object sender, EventArgs e)
        {
            string USERNAME = Username.Text;
            string EMAIL = Email.Text;
            string PASSWORD = Password.Text;
            UpdateUser(USERNAME, EMAIL, PASSWORD);
            ShowData();
        }

    }
}