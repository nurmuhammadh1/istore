﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISTORE
{
    public partial class MyPolicy : Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            if (Session["EMAIL"] != null)
            {
                ShowData();
            }
        }

        public void ShowData()
        {
            try
            {
                conn.Open();
                sql = "select *, PRODUCT.NAME FROM POLICY INNER JOIN PRODUCT ON POLICY.PRODUCTID = PRODUCT.PRODUCTID WHERE USERID = "+Session["USERID"]+ "";
                cmd = new SqlCommand(sql, conn);
                conn.Close();
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowPolicy.DataSource = dt;
                ShowPolicy.DataBind();
            }
            catch (SqlException ex)
            {

            }
        }

        protected void UpdatePolicy(string PAYMENTFILE, string STATUS, string POLICYNO, int USERID)
        {
            conn.Open();
            try
            {
                cmd.CommandText = "select COUNT(*) FROM POLICY WHERE PAYMENTFILE = '' AND USERID = " + Session["USERID"] + "";
                int stat = Convert.ToInt32(cmd.ExecuteScalar());

                if (stat == 1)
                {
                   
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE POLICY SET PAYMENTFILE = '" + PAYMENTFILE + "', STATUS = '" + STATUS + "' WHERE POLICYNO =  '" + POLICYNO + "' AND USERID = "+USERID+ "";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
                else
                {
                     cmd.Connection = conn;
                     ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "warningalert();", true);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            
            UpdatePolicy(Payment.Text, "Inforce", Policyno.Text, Convert.ToInt32(Session["USERID"]));
            clear();
        }

        public void clear()
        {
            Payment.Text = string.Empty;
            Status.Text = string.Empty;
            Policyno.Text = string.Empty;
        }

    }
}