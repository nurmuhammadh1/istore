﻿<%@ Page Title="Claim" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Claim.aspx.cs" Inherits="ISTORE.Claim" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="mt-4"><%: Title %></h2>

    <div class="row mt-4">
        <h3 class="mt-3">Your Policy Inforce</h3>
        <asp:ListView ID="ShowPolicy" runat="server">
            <LayoutTemplate>
                <table class="table table-striped">
                    <tr>
                        <td>POLICY NO</td>
                        <td>STATUS</td>
                        <td>BENEFIT</td>
                        <td>MIN CLAIM</td>
                        <td>MAX CLAIM</td>
                        <td>ACTION</td>
                    </tr>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td><%# Eval("PRODUCTID") %>-<%# Eval("POLICYNO") %></td>
                    <td><%# Eval("STATUS") %></td>
                    <td><%# Eval("DESCRIPTION") %></td>
                    <td><%# String.Format("{0:C}",  Eval("MINAMOUNT")) %></td>
                    <td><%# String.Format("{0:C}",  Eval("MAXAMOUNT")) %></td>
                    <td>
                        <a id="edit_Policy" class="btn btn-primary" data-toggle="modal"
                            data-policyno="<%# Eval("POLICYNO") %>" data-productid="<%# Eval("PRODUCTID") %>"
                            data-benefitid="<%# Eval("BENEFITID") %>" data-insuredname="<%# Eval("INSUREDNAME") %>"
                            data-benefittype="<%# Eval("BENEFITTYPE") %>" data-maxamount="<%# Eval("MAXAMOUNT") %>"
                            data-minamount="<%# Eval("MINAMOUNT") %>"
                            data-target="#exampleModal">Claim Capture
                        </a>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div class="row mt-4">
        <h3 class="mt-4">Your Claim List</h3>
        <asp:ListView ID="ShowClaim" runat="server">
            <LayoutTemplate>
                <table class="table table-striped">
                    <tr>
                        <td>POLICY NO</td>
                        <td>CLAIMANT</td>
                        <td>REASON</td>
                        <td>TOTAL AMOUNT</td>
                        <td>STATUS CLAIM</td>
                        <td>ACTION</td>
                    </tr>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td><%# Eval("PRODUCTID") %>-<%# Eval("POLICYNO") %></td>
                    <td><%# Eval("INSUREDNAME") %></td>
                    <td><%# Eval("BENEFITTYPE") %></td>
                    <td><%# String.Format("{0:C}",  Eval("AMOUNT")) %></td>
                    <td><%# Eval("STATUSCLAIM") %></td>
                    <td><a id="Remove_Claim" data-claimno="<%# Eval("CLAIMNO") %>" class="btn btn-danger"
                        data-toggle="modal" data-target="#exampleModalRemove">Remove</a></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header alert-primary">
                    <h5 class="modal-title" id="exampleModalLabel">Claim Capture</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:Label hidden for="id" ID="Label1" runat="server" Text="Payment Proof"></asp:Label>
                        <asp:TextBox hidden ClientIDMode="Static" ID="Benefitid" CssClass="form-control mt-2" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label hidden for="id" ID="Label6" runat="server" Text="Payment Proof"></asp:Label>
                        <asp:TextBox hidden ClientIDMode="Static" ID="Maxamount" CssClass="form-control mt-2" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label hidden for="id" ID="Label8" runat="server" Text="Payment Proof"></asp:Label>
                        <asp:TextBox hidden ClientIDMode="Static" ID="Minamount" CssClass="form-control mt-2" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <asp:Label for="id" ID="Label3" runat="server" Text="Policy No"></asp:Label>
                            <asp:TextBox ClientIDMode="Static" ID="Productid" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-3 mt-4">
                            <asp:TextBox ClientIDMode="Static" ID="Policyno" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Claimant"></asp:Label>
                        <asp:TextBox ClientIDMode="Static" ID="Insuredname" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="Amount"></asp:Label>
                        <asp:TextBox type="number" ID="Amount" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text="Reason"></asp:Label>
                            <asp:TextBox ClientIDMode="Static" ID="Benefittype" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <asp:Button ID="submitclaim" OnClick="submitclaim_Click" CssClass="btn btn-primary" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModalRemove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header alert-primary">
                    <h5 class="modal-title" id="exampleModalLabelRemove">Have You Received a Claim?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:Label hidden for="id" ID="Label7" runat="server" Text="Payment Proof"></asp:Label>
                        <asp:TextBox hidden ClientIDMode="Static" ID="Claimno" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <p>Thank you for making a claim, please reapply later</p>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <asp:Button ID="RemoveClaik" OnClick="DeleteClaim_Click" CssClass="btn btn-danger" runat="server" Text="Remove" />
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#myPolicy').DataTable();
        });
        $(document).on("click", "#edit_Policy", function () {
            var _policyno = $(this).data("policyno");
            var _productid = $(this).data("productid");
            var _benefitid = $(this).data("benefitid");
            var _insuredname = $(this).data("insuredname");
            var _benefittype = $(this).data("benefittype");
            var _maxamount = $(this).data("maxamount");
            var _minamount = $(this).data("minamount");

            $("#exampleModal #Policyno").val(_policyno);
            $("#exampleModal #Policyno").attr("ReadOnly", true);
            $("#exampleModal #Productid").val(_productid);
            $("#exampleModal #Productid").attr("ReadOnly", true);
            $("#exampleModal #Benefitid").val(_benefitid);
            $("#exampleModal #Benefitid").attr("ReadOnly", true);
            $("#exampleModal #Insuredname").val(_insuredname);
            $("#exampleModal #Insuredname").attr("ReadOnly", true);
            $("#exampleModal #Benefittype").val(_benefittype);
            $("#exampleModal #Benefittype").attr("ReadOnly", true);
            $("#exampleModal #Maxamount").val(_maxamount);
            $("#exampleModal #Maxamount").attr("ReadOnly", true);
            $("#exampleModal #Minamount").val(_minamount);
            $("#exampleModal #Minamount").attr("ReadOnly", true);

        })
    </script>

    <script>
        $(document).ready(function () {
            $('#myClaim').DataTable();
        });
        $(document).on("click", "#Remove_Claim", function () {
            var _claimno = $(this).data("claimno");
            

            $("#exampleModalRemove #Claimno").val(_claimno);
            $("#exampleModalRemove #Claimno").attr("ReadOnly", true);

        })
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function successalert() {
            swal({
                icon: 'success',
                title: 'Success',
                text: 'Your claim has been submited, waiting for approve from admin',
                type: 'success'
            });
        }
    </script>

    <script type="text/javascript">
        function warningalert() {
            swal({
                icon: 'warning',
                title: 'Warning',
                text: 'You have already submitted a claim, please wait for the next claim submission or remove your claim before',
                type: 'warning'
            });
        }
    </script>

    <script type="text/javascript">
        function erroralert() {
            swal({
                icon: 'error',
                title: 'Error',
                text: 'Claims you submit exceed the maximum or minimum claim, please file correctly',
                type: 'error'
            });
        }
    </script>

    <script type="text/javascript">
        function successremove() {
            swal({
                icon: 'success',
                title: 'Success',
                text: 'Thank you, please reapplay claim later ',
                type: 'success'
            });
        }
    </script>
</asp:Content>
