﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace ISTORE
{
    public partial class CheckOut : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            if (Session["EMAIL"] != null)
            {
                string id = Request.QueryString["PRODUCTID"];
                ShowData(id);
                ShowDataUser();
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            string numbers = "1234567890";
            string characters = numbers;

            string id = string.Empty;
            for (int i = 0; i < 5; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (id.IndexOf(character) != -1);
                id += character;
            }
            policyno.Text = id;
        }

        public void ShowData(string id)
        {
            try
            {
                conn.Open();
                sql = $"select *, BENEFIT.DESCRIPTION FROM PRODUCT INNER JOIN BENEFIT ON PRODUCT.BENEFITID = BENEFIT.BENEFITID WHERE PRODUCT.PRODUCTID = '{id}' ";
                cmd = new SqlCommand(sql, conn);
                conn.Close();
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowCart.DataSource = dt;
                ShowCart.DataBind();
                ShowCart1.DataSource = dt;
                ShowCart1.DataBind();
            }
            catch (SqlException ex)
            {

            }
        }

        public void ShowDataUser()
        {
            try
            {
                conn.Open();
                sql = "select USERNAME from [USER] where USERID = " + Session["USERID"] + "";
                cmd = new SqlCommand(sql, conn);
                conn.Close();
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowUser.DataSource = dt;
                ShowUser.DataBind();

            }
            catch (SqlException ex)
            {

            }
        }

        protected void CreatePolicy(string POLICYNO, string INSUREDNAME, string STATUS, string PRODUCTID, string PAYMENTFILE, int USERID)
        {
            conn.Open();
            try
            {
                cmd.CommandText = "SELECT COUNT(*) FROM POLICY WHERE PRODUCTID = '" + PRODUCTID + "' AND USERID = " + USERID + "";
                int validation = Convert.ToInt32(cmd.ExecuteScalar());


                if (validation > 0)
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM CART WHERE PRODUCTID = '" + PRODUCTID + "'";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
                else
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO POLICY VALUES('" + POLICYNO + "', '" + INSUREDNAME + "', '" + STATUS + "', '" + PRODUCTID + "', '" + PAYMENTFILE + "', " + USERID + " )";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }


        protected void btnCreate_Click(object sender, EventArgs e)
        {
            string PRODUCTID = Request.QueryString["PRODUCTID"];

            CreatePolicy(Convert.ToString(policyno.Text), Convert.ToString(Session["USERNAME"]), "Pending", PRODUCTID, null, Convert.ToInt32(Session["USERID"]));
            ShowData(PRODUCTID);
            ShowDataUser();
        }

    }
}