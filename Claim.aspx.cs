using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISTORE
{
    public partial class Claim : Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            if (Session["EMAIL"] != null)
            {
                PolicyInforce();
                ClaimList();
            }
            
        }

        public void PolicyInforce()
        {
            try
            {
                conn.Open();
                sql = "SELECT *, PO.PRODUCTID, POLICYNO, STATUS, BE.DESCRIPTION, BE.MAXAMOUNT, BE.MINAMOUNT FROM POLICY PO  " +
                    "INNER JOIN PRODUCT PR ON PO.PRODUCTID = PR.PRODUCTID " +
                    "INNER JOIN BENEFIT BE ON BE.PRODUCTID = PR.PRODUCTID WHERE PO.STATUS = 'Inforce' AND USERID = " + Session["USERID"] + "";
                cmd = new SqlCommand(sql, conn);
                conn.Close();
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowPolicy.DataSource = dt;
                ShowPolicy.DataBind();
            }
            catch (SqlException ex)
            {

            }
        }


        public void ClaimList()
        {
            try
            {
                conn.Open();
                sql = "SELECT *, PO.INSUREDNAME, BE.BENEFITTYPE FROM CLAIM CL " +
                    "INNER JOIN PRODUCT PR ON CL.PRODUCTID = PR.PRODUCTID " +
                    "INNER JOIN BENEFIT BE ON BE.PRODUCTID = PR.PRODUCTID " +
                    "INNER JOIN POLICY PO ON CL.POLIYCNO = PO.POLICYNO " +
                    "WHERE CL.USERID = " + Session["USERID"] + "";
                cmd = new SqlCommand(sql, conn);
                conn.Close();
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowClaim.DataSource = dt;
                ShowClaim.DataBind();
            }
            catch (SqlException ex)
            {

            }
        }

        

        protected void SubmitClaim(string CLAIMANT, decimal AMOUNT, string STATUSCLAIM, int BENEFITID, string PRODUCTID, string REASON, int POLICYNO, int USERID)
        {
            conn.Open();
            try
            {
                cmd.CommandText = "SELECT COUNT(*) FROM CLAIM WHERE PRODUCTID = '" + PRODUCTID + "' AND USERID = "+USERID+"";
                int validation = Convert.ToInt32(cmd.ExecuteScalar());

                if (validation == 0)
                {
                    decimal maxamount = Convert.ToDecimal(Maxamount.Text);
                    decimal minamount = Convert.ToDecimal(Minamount.Text);
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO CLAIM VALUES('" + CLAIMANT + "', " + AMOUNT + ", '" + STATUSCLAIM + "', " + BENEFITID + ", '" + PRODUCTID + "', '" + REASON + "', " + POLICYNO + ", " + USERID + ")";
                    
                    if (AMOUNT > maxamount)
                    {
                        cmd.CommandText = "DELETE FROM CLAIM WHERE PRODUCTID = '" + PRODUCTID + "' AND USERID = "+Session["USERID"]+" ";
                        cmd.ExecuteNonQuery();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                    }
                    if (AMOUNT < minamount)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                    }
                    else
                    {
                        cmd.ExecuteNonQuery();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "warningalert();", true);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        protected void submitclaim_Click(object sender, EventArgs e)
        {
            SubmitClaim(Insuredname.Text, Convert.ToDecimal(Amount.Text), "Pending", Convert.ToInt32(Benefitid.Text), Productid.Text, Benefittype.Text, Convert.ToInt32(Policyno.Text), Convert.ToInt32(Session["USERID"]));
            clear();
        }

        protected void RemoveClaim(int CLAIMNO)
        {
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE FROM CLAIM WHERE CLAIMNO = " + CLAIMNO + "";
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        protected void DeleteClaim_Click(object sender, EventArgs e)
        {
            RemoveClaim(Convert.ToInt32(Claimno.Text));
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successremove();", true);
        }

        public void clear()
        {
            Amount.Text = string.Empty;
        }

    }
}
