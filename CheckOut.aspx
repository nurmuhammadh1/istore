﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckOut.aspx.cs" CodeFile="CheckOut.aspx.cs" Inherits="ISTORE.CheckOut" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1 class="mt-3">Check Out</h1>

        <div class="form-row">
            <asp:Repeater ID="ShowCart" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    <div class="form-group col-md-2">
                        <label class="mt-3">Policy No</label>
                        <input type="text" id="productid" readonly="true" value="<%#Eval("PRODUCTID") %>" class="form-control" id="">
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="form-group col-md-3 mt-4">
                <label></label>
                <asp:TextBox ID="policyno" required="true" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <asp:Button CssClass="col-1 mt-5" Height="40" ID="btnGenerate" OnClick="btnGenerate_Click" runat="server" Text="Get ID" />
        </div>
        <asp:Repeater ID="ShowUser" runat="server">
            <ItemTemplate>
                <div class="form-group">
                    <label>Insured Name</label>
                    <input type="text" id="productid" readonly="true" value="<%#Eval("USERNAME") %>" class="form-control">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="form-group">
            <label>Status Policy</label>
            <asp:TextBox value="Pending" ReadOnly="true" ClientIDMode="Static" ID="Status" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <asp:Repeater ID="ShowCart1" runat="server">
            <ItemTemplate>
                <div class="form-group">
                    <label>Premium</label>
                    <input type="text" id="productid" readonly="true" value="<%#Eval("PREMIUM") %>" class="form-control" id="">
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Button ID="btnCreate" OnClick="btnCreate_Click" CssClass="btn btn-primary" runat="server" Text="Entry Policy" />

    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function successalert() {
            swal({
                icon: 'success',
                title: 'Success',
                text: 'Your policy has been added, please doing the payment for inforce status',
                type: 'success'
            }).then(function () {
                window.location = "MyPolicy.aspx"
            });
        }
    </script>
    <script type="text/javascript">
        function erroralert() {
            swal({
                icon: 'error',
                title: 'Error',
                text: 'You cannot buy this product because you already have policy in this product, please upload your payment',
                type: 'error'
            }).then(function () {
                window.location = "MyPolicy.aspx"
            });
        }
    </script>
</asp:Content>
