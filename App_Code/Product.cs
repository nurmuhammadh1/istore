﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ISTORE.App_Code
{
    public class Product
    {
        public string PRODUCTID { get; set; }
        public decimal PREMIUM { get; set; }

        public Product(string detail)
        {
            this.PRODUCTID = detail;
            switch (detail)
            {
                case "PRODUCTID":
                    string constr = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                    string query = $"select *, BENEFIT.DESCRIPTION FROM PRODUCT INNER JOIN BENEFIT ON PRODUCT.BENEFITID = BENEFIT.BENEFITID WHERE PRODUCT.PRODUCTID = '{detail}' ";
                    using (SqlConnection con = new SqlConnection(constr))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(query, con))
                        {
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                            }
                        }
                    }
                    break;
            }


        }
    }
}

