﻿using System;

namespace ISTORE.App_Code
{
    public class CartItem : IEquatable<CartItem>
    {
        #region Properties

        // A place to store the quantity in the cart
        // This property has an implicit getter and setter.
        public decimal QUANTITY { get; set; }

        private string _productId;
        public string PRODUCTID
        {
            get { return _productId; }
            set
            {
                // To ensure that the Prod object will be re-created
                _product = null;
                _productId = value;
            }
        }

        private Product _product = null;
        public Product Prod
        {
            get
            {
                // Lazy initialization - the object won't be created until it is needed
                if (_product == null)
                {
                    _product = new Product("PRODUCTID");
                }
                return _product;
            }
        }



        public decimal PREMIUM
        {
            get { return Prod.PREMIUM; }
        }

        public decimal TotalPrice
        {
            get { return PREMIUM * QUANTITY; }
        }

        #endregion

        // CartItem constructor just needs a productId
        public CartItem(string productId)
        {
            this.PRODUCTID = productId;
        }

        /**
         * Equals() - Needed to implement the IEquatable interface
         *    Tests whether or not this item is equal to the parameter
         *    This method is called by the Contains() method in the List class
         *    We used this Contains() method in the ShoppingCart AddItem() method
         */
        public bool Equals(CartItem item)
        {
            return item.PRODUCTID == this.PRODUCTID;
        }

    }
}