﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ISTORE.Register" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <div class="title mb-5">
            <center>
                <h1>FORM REGISTER</h1>
            </center>
        </div>
        <div class="form-row">
            <div class="form-group mt-4 col-md-4">
                <label>Full Name</label>
                <asp:TextBox AutoCompleteType="Disabled" required ID="User_name" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group mt-4 col-md-4">
                <label>Email</label>
                <asp:TextBox AutoCompleteType="Disabled" name="Email" TextMode="Email" required ID="Email" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label>Birth Place</label>
                <asp:TextBox AutoCompleteType="Disabled" required ID="BirthPlace" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <label>Password</label>
                <asp:TextBox required TextMode="Password" name="Pass_word" ID="Pass_word" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label>Birth Date</label>
            <asp:TextBox type="date" required ID="BirthDate" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="inputState">Gender</label>
            <select required="required" runat="server" id="Gender" name="Gender" class="form-control">
                <option selected>-- Choose Gender --</option>
                <option value="MALE">MALE</option>
                <option value="FEMALE">FEMALE</option>
            </select>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="exampleFormControlTextarea1">Address</label>
                <textarea class="form-control" required runat="server" id="Address" rows="3"></textarea>
            </div>
            <div class="form-group col-md-2">
                <label>Zip Code</label>
                <asp:TextBox ID="ZipCode" required CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 text-right">
            <a class="btn btn-danger" runat="server" href="~/">Cancel</a>
            
            <asp:Button ID="btnCreateUser" OnClick="btnCreateUser_Click" CssClass="btn btn-primary" runat="server" Text="Save" />
        </div>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function successalert() {
            swal({
                icon: 'success',
                title: 'Success',
                text: 'Succes, you have registed, please login',
                type: 'success'
            }).then(function () {
                window.location = "default.aspx"
            });
        }
    </script>
    <script type="text/javascript">
        function erroralert() {
            swal({
                icon: 'error',
                title: 'Error',
                text: 'Your email already registed, please use another email',
                type: 'error'
            });
        }
    </script>
 </div>
</asp:Content>
