﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ISTORE._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ISTORE</h1>
        <img src="images/insurance.jpeg" class="card-img-top" width="100%">
    </div>


    <div class="row">
        <asp:Repeater ID="ShowProduct" runat="server">
            <ItemTemplate>
                <div id="myCard" class="col-md-4">
                    <img src="images/<%#Eval("IMAGES") %>" class="card-img-top" width="100%">
                    <h2><%#Eval("NAME") %></h2>
                    <p>
                        <%#Eval("DESCRIPTION") %>
                    </p>

                    <p runat="server">
                        <a class="btn btn-success" href="DetailsProduct.aspx?PRODUCTID=<%# Eval("PRODUCTID") %>">View</a>
                    </p>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function erroralert() {
            swal({
                icon: 'error',
                title: 'Invalid Email & Password',
                text: 'Your Email or Password does not correct, please try again',
                type: 'error'
            });
        }
    </script>
    <script type="text/javascript">
        function successalert() {
            swal({
                icon: 'success',
                title: 'Success Login',
                text: 'Thank You, You already logged click ok to continue',
                type: 'success'
            }).then(function () {
                window.location = "Home.aspx"
            });
        }
    </script>

</asp:Content>
