﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISTORE
{
    public partial class Register : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            ShowData();
        }

        public void ShowData()
        {
            conn.Open();
            try
            {
                sql = "select * from USER";
                cmd = new SqlCommand(sql, conn);
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {

            }
            finally
            {
                conn.Close();
            }
        }

        

        protected void AddUser(user _user)
        {
            conn.Open();
            try
            {
                cmd.CommandText = "SELECT COUNT(*) FROM [USER] WHERE EMAIL = '" + _user.email +"'";
                cmd.Parameters.AddWithValue("@EMAIL", Email.Text.Trim());
                int validation = Convert.ToInt32(cmd.ExecuteScalar());

                if (validation > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                    clear();
                }
                else
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO [USER] VALUES('" + _user.username + "', '" + _user.password + "', '" + _user.birthplace + "', '" + _user.birthdate + "'" +
                        ", '" + _user.gender + "', '" + _user.address + "', '" + _user.zipcode + "', '" + _user.email + "')";
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        protected void btnCreateUser_Click(object sender, EventArgs e)
        {
 
            string USERNAME = User_name.Text;
            string PASSWORD = Pass_word.Text;
            string BIRTHPLACE = BirthPlace.Text;
            string BIRTHDATE = BirthDate.Text;
            string GENDER = Gender.Value;
            string ADDRESS = Address.InnerText;
            string ZIPCODE = ZipCode.Text;
            string EMAIL = Email.Text;

            user _user = new user();
            _user.username = USERNAME;
            _user.password = PASSWORD;
            _user.birthplace = BIRTHPLACE;
            _user.birthdate = BIRTHDATE;
            _user.gender = GENDER;
            _user.address = ADDRESS;
            _user.zipcode = ZIPCODE;
            _user.email = EMAIL;

            AddUser(_user);
            ShowData();
            clear();
        }

        public void clear()
        {
            User_name.Text = string.Empty;
            Pass_word.Text = string.Empty;
            BirthPlace.Text = string.Empty;
            BirthDate.Text = string.Empty;
            Gender.Value = "-- Choose Gender --";
            Address.InnerText = string.Empty;
            ZipCode.Text = string.Empty;
            Email.Text = string.Empty;
        }
    }

    public class user
    {           
        public string username { get; set; }
        public string password { get; set; }
        public string birthplace { get; set; }
        public string birthdate { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public string zipcode { get; set; }
        public string email { get; set; }
    }
}