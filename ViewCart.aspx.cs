﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace ISTORE
{
    public partial class ViewCart : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd;
        public SqlDataAdapter da;
        public DataTable dt;
        string sql, sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["Conn"].ToString();
            conn = new SqlConnection(sqlcon);
            ShowData();
        }

        protected void RemoveToCart(int CARTID)
        {
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE FROM CART WHERE CARTID = '" + CARTID + "'";
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            RemoveToCart(Convert.ToInt32(Cartid.Text));
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
            
            ShowData();
        }

        public void ShowData()
        {
            try
            {
                conn.Open();
                sql = "select *, PRODUCT.NAME FROM CART INNER JOIN PRODUCT ON CART.PRODUCTID = PRODUCT.PRODUCTID WHERE USERID = " + Session["USERID"] + " ";
                cmd = new SqlCommand(sql, conn);
                conn.Close();
                da = new SqlDataAdapter();
                dt = new DataTable();
                da.SelectCommand = cmd;
                SqlCommandBuilder sbc = new SqlCommandBuilder(da);
                da.Fill(dt);
                ShowCart.DataSource = dt;
                ShowCart.DataBind();
            }
            catch (SqlException ex)
            {

            }
        }
    }
}